//
//  PokemonDataServices_Test.swift
//  PokeAppTests
//
//  Created by Divier on 4/2/2024.
//

import XCTest
@testable import PokeApp


final class PokemonDataServices_Test: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    //Given
    //When
    //Them
    func test_PokemonDataServices_GetPokemonesByOffset_Success() {
        let pokemonServices: PokemonDataServicesProtocol = PokemonDataServices()
        
        let initialOffset = 0
        let expectation = expectation(description: "waiting any response")
        var newItems: [PokemonViewModel]?
        
        pokemonServices.getPokemonesByPage(offSet: initialOffset) { itemsReturned in
            newItems = itemsReturned
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 3)
        XCTAssertNotNil(newItems)
        XCTAssertNotEqual(newItems!.count, 0, "")
        XCTAssertTrue(newItems!.count > 1)
        
    }

    
    func test_PokemonDataServices_GetPokemonesByOffset_Fail() {
        let pokemonServices: PokemonDataServicesProtocol = PokemonDataServices()
        
        let initialOffset = -1
        let expectation = expectation(description: "waiting any response")
        var newItems: [PokemonViewModel]?
        
        pokemonServices.getPokemonesByPage(offSet: initialOffset) { itemsReturned in
            newItems = itemsReturned
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 3)
        XCTAssertNotNil(newItems)
        XCTAssertEqual(newItems?.count, 0)
        
    }
    
    func test_PokemonDataServices_GetPokemonDetailBy_Success() {
        let pokemonServices: PokemonDataServicesProtocol = PokemonDataServices()
        
        let url = "https://pokeapi.co/api/v2/pokemon/1"
        let expectation = expectation(description: "waiting any response")
        var newItem: PokemonViewModel?
        
        pokemonServices.getPokemonDetailBy(url: url) { response in
            newItem = response
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 3)
        XCTAssertNotNil(newItem)
        XCTAssertNotNil(newItem!.id)
        XCTAssertNotEqual(newItem!.abilities.count, 0)
        XCTAssertNotEqual(newItem!.moves.count, 0)
        
    }
    
    func test_PokemonDataServices_GetPokemonDetailBy_Fail() {
        let pokemonServices: PokemonDataServicesProtocol = PokemonDataServices()
        
        let url = "https://pokeapi.co/api/v2/pokemon/-11"
        
        var newItem: PokemonViewModel?
        
        pokemonServices.getPokemonDetailBy(url: url) { response in
            // No one response
            newItem = response
        }
        sleep(3)
        
        XCTAssertNil(newItem)
        XCTAssertNil(newItem?.id)
        XCTAssertEqual(newItem?.abilities.count, nil)
        XCTAssertEqual(newItem?.moves.count, nil)
    }
}

//
//  PokemonItemRowViewTest.swift
//  PokeAppTests
//
//  Created by Divier on 6/2/2024.
//

import XCTest
import SnapshotTesting
@testable import PokeApp

final class PokemonItemRowViewTest: XCTestCase {
    
    var pokemon: PokemonViewModel?
    
    override func setUpWithError() throws {
        
    }
    
    override func tearDownWithError() throws {
        pokemon = nil
    }
    
    func test_ListView() throws {
        
        let homeViewModel = HomeViewModel(pokemonDataServices: nil)
        homeViewModel.pokemonToShow.append(buildPokemonViewModel(style: .water))
        homeViewModel.pokemonToShow.append(buildPokemonViewModel(style: .water))
         
        let homeView = HomeView(homeViewModel: homeViewModel, navController: NavigationController())
        
        assertSnapshots(of: homeView, as: [.image], testName: "PokemonItemRowViewTest_List")
    }
    
    func test_ItemRowView_Bug() throws {
        pokemon = buildPokemonViewModel(style: .bug)
        
        let homeViewModel = HomeViewModel(pokemonDataServices: nil)
        homeViewModel.pokemonToShow = [pokemon!]
        let homeView = HomeView(homeViewModel: homeViewModel, navController: NavigationController())
        
        assertSnapshots(of: homeView, as: [.image], testName: "PokemonItemRowViewTest_bug")
    }
    
    func test_ItemRowView_Dragon() throws {
        pokemon = buildPokemonViewModel(style: .dragon)
        
        let homeViewModel = HomeViewModel(pokemonDataServices: nil)
        homeViewModel.pokemonToShow = [pokemon!]
        let homeView = HomeView(homeViewModel: homeViewModel, navController: NavigationController())
        
        assertSnapshots(of: homeView, as: [.image], testName: "PokemonItemRowViewTest_dragon")
    }
    
    func test_ItemRowView_Electric() throws {
        pokemon = buildPokemonViewModel(style: .electric)
        
        let homeViewModel = HomeViewModel(pokemonDataServices: nil)
        homeViewModel.pokemonToShow = [pokemon!]
        let homeView = HomeView(homeViewModel: homeViewModel, navController: NavigationController())
        
        assertSnapshots(of: homeView, as: [.image], testName: "PokemonItemRowViewTest_electric")
    }
    
    func test_ItemRowView_Fire() throws {
        pokemon = buildPokemonViewModel(style: .fire)
        
        let homeViewModel = HomeViewModel(pokemonDataServices: nil)
        homeViewModel.pokemonToShow = [pokemon!]
        let homeView = HomeView(homeViewModel: homeViewModel, navController: NavigationController())
        
        assertSnapshots(of: homeView, as: [.image], testName: "PokemonItemRowViewTest_fire")
    }
    
    func test_ItemRowView_Flying() throws {
        pokemon = buildPokemonViewModel(style: .flying)
        
        let homeViewModel = HomeViewModel(pokemonDataServices: nil)
        homeViewModel.pokemonToShow = [pokemon!]
        let homeView = HomeView(homeViewModel: homeViewModel, navController: NavigationController())
        
        assertSnapshots(of: homeView, as: [.image], testName: "PokemonItemRowViewTest_flying")
    }
    
    func test_ItemRowView_Grass() throws {
        pokemon = buildPokemonViewModel(style: .grass)
        
        let homeViewModel = HomeViewModel(pokemonDataServices: nil)
        homeViewModel.pokemonToShow = [pokemon!]
        let homeView = HomeView(homeViewModel: homeViewModel, navController: NavigationController())
        
        assertSnapshots(of: homeView, as: [.image], testName: "PokemonItemRowViewTest_grass")
    }
    
    func test_ItemRowView_Normal() throws {
        pokemon = buildPokemonViewModel(style: .normal)
        
        let homeViewModel = HomeViewModel(pokemonDataServices: nil)
        homeViewModel.pokemonToShow = [pokemon!]
        let homeView = HomeView(homeViewModel: homeViewModel, navController: NavigationController())
        
        assertSnapshots(of: homeView, as: [.image], testName: "PokemonItemRowViewTest_normal")
    }
    
    func test_ItemRowView_Psychic() throws {
        pokemon = buildPokemonViewModel(style: .psychic)
        
        let homeViewModel = HomeViewModel(pokemonDataServices: nil)
        homeViewModel.pokemonToShow = [pokemon!]
        let homeView = HomeView(homeViewModel: homeViewModel, navController: NavigationController())
        
        assertSnapshots(of: homeView, as: [.image], testName: "PokemonItemRowViewTest_psychic")
    }
    
    func test_ItemRowView_Water() throws {
        pokemon = buildPokemonViewModel(style: .water)
        
        let homeViewModel = HomeViewModel(pokemonDataServices: nil)
        homeViewModel.pokemonToShow = [pokemon!]
        let homeView = HomeView(homeViewModel: homeViewModel, navController: NavigationController())
        
        assertSnapshots(of: homeView, as: [.image], testName: "PokemonItemRowViewTest_water")
    }
    
}

extension PokemonItemRowViewTest {
    private func buildPokemonViewModel(style: ItemRowViewStyleEnum) -> PokemonViewModel {
        
        let pokemon = PokemonViewModel(id: 1,
                                       name: "Bulbasaur",
                                       url: "https://pokeapi.co/api/v2/pokemon/1",
                                       mainLiteralImage: UIImage(imageLiteralResourceName: "ImageBulbasaur"),
                                       secondLiteralImage: nil,
                                       mainUrlImage: "",
                                       secondUrlImage: "",
                                       types: ["grass", "poison"],
                                       moves: ["swords-dance", "razor-wind"],
                                       abilities: ["overgrow" , "chlorophyll"],
                                       typeStyle: style)
        
        
        return pokemon
    }
}

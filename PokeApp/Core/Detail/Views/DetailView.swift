//
//  DetailView.swift
//  PokeApp
//
//  Created by Divier on 1/2/2024.
//

import SwiftUI

struct DetailView: View {
    
    var pokemon: PokemonViewModel
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                
                headerView
                
                abilitiesSection
                    .padding(.leading, 12)
                    .frame(maxWidth: .infinity, alignment: .leading)
                
                movesSection
                    .padding(.leading, 12)
                    .frame(maxWidth: .infinity, alignment: .leading)
                
            }
        }
    }
    
}

extension DetailView {
    var headerView: some View {
        VStack {
            if let pokeImage = pokemon.mainLiteralImage {
                Image(uiImage: pokeImage)
                    .resizable()
                    .frame(width: UIScreen.screenWidth / 2, height: UIScreen.screenWidth / 2)
                    .scaledToFit()
            }
            HStack {
                if let typeImage = pokemon.typeStyle?.image {
                    Image(uiImage: typeImage)
                        .resizable()
                        .frame(width: 30, height: 30, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .padding(.leading)
                }
                Text(pokemon.name)
                    .font(.system(size: 36, weight: .bold, design: .monospaced))
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .frame(height: 55)
                    .foregroundStyle(Color.colorTitleText.opacity(0.8))
                    
            }
        }
    }
}

extension DetailView {
    var abilitiesSection: some View {
        VStack(alignment:.leading) {
            Text("Abilities")
                .font(.system(size: 20))
                .padding(.top)
            ScrollView(.horizontal) {
                HStack {
                    ForEach(pokemon.abilities, id: \.self) { item in
                        LabelView(text: item, color: .green)
                    }
                }
                .font(.system(size: 28))
            }
            .menuIndicator(.hidden)
        }
    }
    
    var movesSection: some View {
        VStack(alignment:.leading) {
            Text("Moves")
                .frame(maxWidth: .infinity, alignment: .leading)
                .frame(height: 15)
                .font(.system(size: 20))
                .padding(.top)
            
            ScrollView(.horizontal) {
                HStack {
                    ForEach(pokemon.moves, id: \.self) { item in
                        LabelView(text: item, color: .blue)
                    }
                }
                .font(.system(size: 28))
            }
            .menuIndicator(.hidden)
        }
        
    }
}

#Preview {
    DetailView(pokemon: PokemonViewModel(DeveloperPreview.instance.pokemon1))
}

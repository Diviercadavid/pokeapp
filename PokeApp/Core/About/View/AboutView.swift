//
//  SwiftUIView.swift
//  PokeApp
//
//  Created by Divier on 1/2/2024.
//

import SwiftUI

struct AboutView: View {
    let linkedInURL = "https://www.linkedin.com/in/diviercadavid/"
    let pokeapiURL = "https://pokeapi.co"
    let imageLibrary = "https://github.com/SDWebImage/SDWebImage"
    
    var body: some View {
        ScrollView(.vertical) {
            
            VStack() {
                Text("About PokeApp")
                    .font(.largeTitle)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .opacity(0.8)
                    .padding()
                
                Section {
                    VStack(alignment: .leading, spacing: 8) {
                        Image("icon")
                        Text("This app was built with the latest Swift version using the most popular API about Pokemon using  URLSession to get data and Combine to show in our views, for showing images they are working with SDWebImage. Also we are using CoreData like database to save these information in local storage. Finally we implement Swift-Snapshot-testing to validate our views with some cases in uitest \nDeveloped by Divier Cadavid")
                            .font(.system(size: 18, weight: .light, design: .default))
                        Button(linkedInURL) {
                            openLink(string: linkedInURL)
                        }
                    }
                    .padding()
                    .frame(maxWidth: .infinity)
                    .overlay(
                        RoundedRectangle(cornerRadius: 20)
                            .stroke(Color.gray.opacity(0.5), lineWidth: 1)
                            .shadow(radius: 10)
                    ).padding()
                }
                
                Section {
                    VStack(alignment: .leading, spacing: 8) {
                        AsyncImage(url: URL(string:"https://raw.githubusercontent.com/PokeAPI/media/master/logo/pokeapi_256.png") )
                        Text("This website provides a RESTful API interface to highly detailed objects built from thousands of lines of data related to Pokémon.")
                            .font(.system(size: 18, weight: .semibold, design: .default))
                        Button(pokeapiURL) {
                            openLink(string: pokeapiURL)
                        }
                    }
                    .padding()
                    .frame(maxWidth: .infinity)
                    .overlay(
                        RoundedRectangle(cornerRadius: 20)
                            .stroke(Color.gray.opacity(0.5), lineWidth: 1)
                            .shadow(radius: 10)
                    ).padding()
                }
                
                Section {
                    
                    VStack(alignment: .leading, spacing: 8) {
                        AsyncImage(url: URL(string: "https://raw.githubusercontent.com/SDWebImage/SDWebImage/master/SDWebImage_logo.png"), content: { image in
                            image.image?.resizable()
                        })
                        .frame(height: 100, alignment: .center)
                        
                        Text("This library provides an async image downloader with cache support. For convenience, we added categories for UI elements like UIImageView, UIButton, MKAnnotationView.")
                            .font(.system(size: 18, weight: .semibold, design: .default))
                        Button(imageLibrary) {
                            openLink(string: imageLibrary)
                        }
                    }
                    .padding()
                    .frame(maxWidth: .infinity)
                    .overlay(
                        RoundedRectangle(cornerRadius: 20)
                            .stroke(Color.gray.opacity(0.5), lineWidth: 1)
                            .shadow(radius: 10)
                    ).padding()
                }
            }
        }
        
    }
}

extension AboutView {
    
    func openLink(string: String) {
        if let url = URL(string: string) {
            UIApplication.shared.open(url)
        }
    }
}

#Preview {
    AboutView()
}

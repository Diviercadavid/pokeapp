//
//  HomeViewModel.swift
//  PokeApp
//
//  Created by Divier on 31/1/2024.
//

import Foundation
import Combine

protocol HomeViewModelObserversProtocol {
    var cancellables: Set<AnyCancellable> { get set }
    var itemsPerPage: Int {get}
    
    func subscribeObservers()
    func loadNextPage()
}

protocol HomeViewModelServicesProtocol {
    func loadPokemonesBy(offSet: Int)
    func getPokemon(url: String)
    func getPokemonImage(pokeId: Int?, urls: [String]?)
}

class HomeViewModel: ObservableObject {
    
    @Published var searchText = ""
    
    @Published var pokemonList: [PokemonViewModel] = []
    @Published var pokemonToShow: [PokemonViewModel] = []
    @Published var isLoading: Bool = false
    @Published var pokemonToUpdate: PokemonViewModel?
    
    private var isSearching: Bool = false
    private var pokemonService: PokemonDataServicesProtocol?
    private var imageServices: ImagesDataServicesProtocol = ImagesDataServices()
    internal var cancellables = Set<AnyCancellable>()
    
    private var requestStack:[String] = []
    private var currentPage = 1
    internal var itemsPerPage: Int { NetworkingManager.itemsPerPage }
    
    init(pokemonDataServices: PokemonDataServicesProtocol?) {
        self.pokemonService = PokemonDataProvider(pokemonService: pokemonDataServices)
        self.loadPokemonesBy(offSet: 0)
        self.subscribeObservers()
    }
}

// MARK: Subscribers section
extension HomeViewModel: HomeViewModelObserversProtocol {
    
    func subscribeObservers() {
        
        $searchText
            .dropFirst()
            .combineLatest($pokemonList)
            .map(pokemonFilter)
            .delay(for: .seconds(1) , scheduler: DispatchQueue.main)
            .sink { [weak self] values in
                self?.pokemonToShow = values
                
            }
            .store(in: &cancellables)
        
        $pokemonToUpdate
            .dropFirst()
            .sink { [weak self] valueReturned in
                guard let self = self,
                      let valueReturned = valueReturned,
                      let index = self.pokemonList.firstIndex(where: {$0.name.lowercased() == valueReturned.name.lowercased()})
                else { return }
                
                self.pokemonList[index] = valueReturned
                validateItemToShow(pokemon: valueReturned)
                self.getPokemonImage(pokeId: valueReturned.id, urls: [valueReturned.mainUrlImage, valueReturned.secondUrlImage])
            }
            .store(in: &cancellables)
        
    }
    
    func pokemonFilter(text: String, originalPokemonList: [PokemonViewModel]) -> [PokemonViewModel] {
        self.isSearching =  !self.searchText.isEmpty
        guard !text.isEmpty else { return pokemonList }
        
        let searchingText = text.lowercased()
        
        return originalPokemonList.filter({
            ($0.name.lowercased().contains(searchingText)) ||
            ($0.abilities.map({$0.lowercased()}).contains(searchingText)) ||
            ($0.types.map({$0.lowercased()}).contains(searchingText))
        })
    }
    
    
    func validateItemToShow(pokemon: PokemonViewModel) {
        guard !pokemon.name.isEmpty, !pokemon.types.isEmpty else { return }
        self.pokemonToShow.append(pokemon)
    }
    
    func loadNextPage() {
        guard Reachability.isConnectedToNetwork() else { return }
        let offSet = (currentPage * itemsPerPage) + 1
        if offSet > pokemonList.count {
            currentPage += 1
            self.isLoading = true
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                self.loadPokemonesBy(offSet: offSet)
            }
        }
    }
}

// MARK: Services Callback section
extension HomeViewModel: HomeViewModelServicesProtocol {
    
    func loadPokemonesBy(offSet: Int) {
        
        guard !isSearching else { return }
        pokemonService?.getPokemonesByPage(offSet: offSet) { [weak self] result in
            guard let self = self, Reachability.isConnectedToNetwork()
            else {
                self?.pokemonToShow = result
                return
            }
            self.isLoading = false
            self.pokemonList.append(contentsOf: result)
            prepareToGetDetails(urls: result.map({$0.url ?? "" }))
        }
    }
    
    func getPokemon(url: String) {
        guard !url.isEmpty else { return }
        self.pokemonService?.getPokemonDetailBy(url: url) { [weak self] valueReturned in
            self?.pokemonToUpdate = valueReturned
            self?.validateRequestStack(urlToRemove: url)
            self?.saveDataToStorage(valueReturned)
        }
    }
    
    func getPokemonImage(pokeId: Int?, urls: [String]?) {
        guard let pokeId = pokeId,
              let urls = urls
        else { return }
        
        imageServices.download(imageURL: urls.first) { [weak self] imageReturned in
            guard let self = self,
                  Reachability.isConnectedToNetwork(),
                  let index = self.pokemonList.lastIndex(where: {$0.id == pokeId})
            else { return }
            
            self.pokemonList[index].setImages(mainImage: imageReturned)
            self.pokemonToShow[index].setImages(mainImage: imageReturned)
        }
    }
    
}

// MARK: Private methods section
extension HomeViewModel {
    
    private func prepareToGetDetails(urls: [String]) {
        self.requestStack = urls
        getPokemon(url: requestStack.first ?? "")
    }
    
    private func validateRequestStack(urlToRemove: String) {
        self.requestStack.removeAll(where: {$0 == urlToRemove})
        self.getPokemon(url: self.requestStack.first ?? "")
    }
    
    private func saveDataToStorage(_ pokemonViewModel: PokemonViewModel) {
        DispatchQueue.main.async {
            StorageDataServices.shared.save(pokemonViewModel: pokemonViewModel)
        }
        
    }
}

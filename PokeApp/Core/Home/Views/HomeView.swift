//
//  HomeView.swift
//  PokeApp
//
//  Created by Divier on 31/1/2024.
//

import SwiftUI

struct HomeView: View {
    
    @StateObject var homeViewModel: HomeViewModel
    @StateObject var navController: NavigationController
    
    var body: some View {
        NavigationStack(path: $navController.path) {
            VStack {
                List {
                    if !Reachability.isConnectedToNetwork() {
                        offlineLabel
                    }
                    
                    headerTextLabel
                        
                    SearchBarView(searchText: $homeViewModel.searchText)
                        .listRowSeparator(.hidden)
                        .frame(height: 30)
                        .listRowInsets(EdgeInsets())
                        .padding(.vertical, 18)
                    
                    listOfItems
                    
                    if homeViewModel.isLoading {
                        loadingMoreLabel
                    }
                    
                }
                .navigationTitle("PokeApp")
                .listStyle(.grouped)
                .navigationDestination(for: PokemonViewModel.self) { value in
                    DetailView(pokemon: value)
                }
                .navigationDestination(for: NavigationType.self, destination: { value in
                    if value == .about {
                        AboutView()
                    }
                })
                .toolbar {
                    ToolbarItem(placement: .topBarLeading) {
                        aboutButton
                    }
                }
            }
            
        }
    }
}

extension HomeView {
    
    private var listOfItems: some View {
        ForEach(homeViewModel.pokemonToShow, id: \.self) { item in
            
            VStack {
                ItemRowView(pokemon: item)
                    .onTapGesture {
                        navController.path.append(item)
                    }
                    .onAppear(perform: {
                        if item.id == homeViewModel.pokemonList.last?.id {
                            homeViewModel.loadNextPage()
                        }
                    })
                    .environmentObject(homeViewModel)
            }
            .listRowInsets(EdgeInsets())
        }
    }
    
    private var offlineLabel: some View {
        VStack {
            Text("You are offline")
                .font(.system(size: 16, weight: .light, design: .default))
                .frame(maxWidth: .infinity)
                .background(Color.red)
        }
        .listRowSeparator(.hidden)
        .listRowInsets(EdgeInsets())
    }
    
    private var loadingMoreLabel: some View {
        HStack {
            Text("Getting next page...")
                .font(.system(size: 16, weight: .light, design: .default))
                .frame(maxWidth: .infinity)
                .background(Color.blue.opacity(0.5))
        }
        .listRowSeparator(.hidden)
        .listRowInsets(EdgeInsets())
    }
    
    private var headerTextLabel: some View {
        VStack {
            Text("User the advanced search to find pokemon by type, weakness, ability and more!")
                .font(.system(size: 18, weight: .light, design: .default))
                .frame(maxWidth: .infinity)
                .foregroundColor(Color.colorBodyText)
        }
        .padding(.horizontal, 12)
        .padding(.vertical, 8)
        .listRowSeparator(.hidden)
        .listRowInsets(EdgeInsets())
    }
    
    private var aboutButton: some View {
        Button {
            navController.navigateTo(value: .about)
        } label: {
            Image(systemName: "info.circle")
                    .foregroundStyle(Color.blue.opacity(0.8))
        }
    }
    
}
#Preview {
    HomeView(homeViewModel: HomeViewModel(pokemonDataServices: PokemonDataServices()), navController: NavigationController())
}

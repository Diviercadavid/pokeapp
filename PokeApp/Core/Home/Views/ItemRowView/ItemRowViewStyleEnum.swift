//
//  ItemRowViewStyleEnum.swift
//  PokeApp
//
//  Created by Divier on 2/2/2024.
//

import Foundation
import UIKit
import SwiftUI

enum ItemRowViewStyleEnum {
    case grass
    case water
    case fire
    case psychic
    case bug
    case electric
    case dragon
    case flying
    case normal
    
    var image: UIImage {
        
        switch self {
        case .grass:
            return UIImage(imageLiteralResourceName: "grassIcon")
        case .water:
            return UIImage(imageLiteralResourceName: "waterIcon")
        case .fire:
            return UIImage(imageLiteralResourceName: "fireIcon")
        case .psychic:
            return UIImage(imageLiteralResourceName: "psychicIcon")
        case .bug:
            return UIImage(imageLiteralResourceName: "bugIcon")
        case .electric:
            return UIImage(imageLiteralResourceName: "electricIcon")
        case .dragon:
            return UIImage(imageLiteralResourceName: "dragonIcon")
        case .normal:
            return UIImage(imageLiteralResourceName: "normalIcon")
        case .flying:
            return UIImage(imageLiteralResourceName: "flyingIcon")
        }
    }
    
    var color: Color {
        switch self {
        case .grass:
            return Color(hex: "70e000")
        case .water:
            return Color(hex: "00b4d8")
        case .fire:
            return Color(hex: "ff4b3e")
        case .psychic:
            return Color(hex: "a663cc")
        case .bug:
            return Color(hex: "adc178")
        case .electric:
            return Color(hex: "ffc300")
        case .dragon:
            return Color(hex: "b79ced")
        case .normal:
            return Color(hex: "e9ecef")
        case .flying:
            return Color(hex: "00a896")
        }
    }

}

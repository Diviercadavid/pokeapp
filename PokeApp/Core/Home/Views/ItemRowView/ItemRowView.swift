//
//  ItemRowView.swift
//  PokeApp
//
//  Created by Divier on 31/1/2024.
//

import SwiftUI

struct ItemRowView: View {
    
    var pokemon: PokemonViewModel
    @State private var abilitiesToShow: [String] = []
    @EnvironmentObject private var homeViewModel: HomeViewModel
    
    var body: some View {
        ZStack {
            Color(pokemon.typeStyle?.color.opacity(0.5) ?? .white)
                .clipShape(RoundedRectangle(cornerRadius: 25.0))
            VStack {
                HStack {
                    nameView
                    typeIconView
                }
                .padding(.top, 12)
                .padding(.horizontal, 12)
                
                HStack {
                    abilitiesView
                    
                    pokeImage
                        .frame(maxWidth: UIScreen.screenWidth)
                }
                
            }
        }
        .frame(height: 150)
        .padding()
        
        
    }
}

extension ItemRowView {
    var pokeImage: some View {
        VStack {
            if let image = pokemon.mainLiteralImage {
                Image(uiImage: image)
                    .resizable()
                    .frame(width: 60, height: 60)
                    .scaledToFit()
                    .clipShape(RoundedRectangle(cornerRadius: 30.0))
                    .shadow(color: .white, radius: 10, x: 0.5)
                    
            } else {
                ProgressView()
                    .frame(width: 98, height: 98)
            }
        }
    }
    
    var nameView: some View {
        Text(pokemon.name)
            .font(.system(size: 28, weight: .semibold, design: .rounded))
            .foregroundStyle(Color.colorTitleText.opacity(0.6))
            .frame(maxWidth: .infinity, alignment: .leading)
            .lineLimit(1)
    }
    
    var abilitiesView: some View {
        VStack(alignment: .leading) {
            
            ForEach(pokemon.types.prefix(2), id: \.self) { item in
                LabelView(text: item, color: pokemon.typeStyle?.color ?? .white, textSize: 12)
            }
        }
        .frame(maxWidth: UIScreen.screenWidth / 2, alignment: .leading)
        .padding(12)
    }
    
    var typeIconView: some View {
        VStack {
            if let image = pokemon.typeStyle?.image {
                Image(uiImage: image)
                    .resizable()
                    .frame(width: 40, height: 40)
                    .opacity(0.5)
                    
            }
        }
    }
}


#Preview(traits: .sizeThatFitsLayout) {
    ItemRowView(pokemon: PokemonViewModel(DeveloperPreview.instance.pokemon1))
        .environmentObject(HomeViewModel(pokemonDataServices: PokemonDataServices()))
}

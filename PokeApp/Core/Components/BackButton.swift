//
//  BackButton.swift
//  PokeApp
//
//  Created by Divier on 1/2/2024.
//

import SwiftUI

struct BackButton: View {
    var body: some View {
        Button(action: {
            
        }, label: {
            Image(systemName: "arrow.backward.circle")
                .font(.system(size: 38))
        })
    }
}

#Preview {
    BackButton()
}

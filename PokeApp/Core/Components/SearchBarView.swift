//
//  SearchBarView.swift
//  PokeApp
//
//  Created by Divier on 4/2/2024.
//


import SwiftUI

struct SearchBarView: View {
    
    @Binding var searchText: String
    
    var body: some View {
        HStack {
            Image(systemName: "magnifyingglass")
                .foregroundColor(searchText.isEmpty ? Color.colorTitleText : Color.colorBodyText)
            
            TextField("search by name, ability or type ", text: $searchText)
                .font(.footnote)
                .foregroundColor(Color.colorBodyText)
                .disableAutocorrection(true)
                .overlay(alignment: .trailing) {
                    Image(systemName: "xmark.circle.fill")
                        .padding()
                        .offset(x: 10)
                        .foregroundColor(Color.colorBodyText)
                        .opacity(searchText.isEmpty ? 0.0 : 1.0)
                        .onTapGesture {
                            searchText = ""
                            UIApplication.shared.endEditing()
                        }
                }
            
        }
        .font(.headline)
        .padding()
        .background(RoundedRectangle(cornerRadius: 25)
            .fill(Color.white)
            .shadow(color: Color.colorBodyText.opacity(0.15), radius: 10))
        .padding()
    }
}

#Preview(traits: .sizeThatFitsLayout) {
    SearchBarView(searchText: .constant("hello"))
}

//
//  SwiftUIView.swift
//  PokeApp
//
//  Created by Divier on 1/2/2024.
//

import SwiftUI

struct LabelView: View {
    var text: String
    var color: Color
    var textSize: Float? = 18
    
    var body: some View {
        VStack {
            Text(text)
                .font(.system(size: CGFloat(textSize!), weight: .light, design: .rounded))
                .opacity(0.6)
        }.padding(10)
            .background(color.opacity(0.5))
            .cornerRadius(20)
            .overlay(
                RoundedRectangle(cornerRadius: 20)
                    .stroke(color, lineWidth: 1)
            )
        
    }
}

#Preview {
    LabelView(text: "Hello, World!", color: .green, textSize: 18)
}

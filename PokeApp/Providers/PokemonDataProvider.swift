//
//  PokemonDataProvider.swift
//  PokeApp
//
//  Created by Divier on 2/2/2024.
//

import Foundation

protocol PokemonDataProviderProtocol {
    func getPokemonesByStore(completion: @escaping ([PokemonViewModel]) -> Void)
}

class PokemonDataProvider {
    
    var pokemonService: PokemonDataServicesProtocol?
    
    init(pokemonService: PokemonDataServicesProtocol? = nil) {
        self.pokemonService = pokemonService
    }
}

extension PokemonDataProvider: PokemonDataProviderProtocol {
    func getPokemonesByStore(completion: @escaping ([PokemonViewModel]) -> Void) {
        let storageData:[PokemonViewModel]? = StorageDataServices.shared.getPokemones()
        completion(storageData ?? [])
    }
}

extension PokemonDataProvider: PokemonDataServicesProtocol {
    func getPokemonesByPage(offSet: Int?, completion: @escaping ([PokemonViewModel]) -> Void) {
        if Reachability.isConnectedToNetwork() {
            pokemonService?.getPokemonesByPage(offSet: offSet, completion: completion)
        } else {
            self.getPokemonesByStore(completion: completion)
        }
    }
    
    func getPokemonDetailBy(url: String, completion: @escaping (PokemonViewModel) -> Void) {
        pokemonService?.getPokemonDetailBy(url: url, completion: completion)
    }
}

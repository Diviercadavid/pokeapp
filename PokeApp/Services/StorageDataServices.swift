//
//  StorageDataServices.swift
//  PokeApp
//
//  Created by Divier on 2/2/2024.
//

import Foundation
import CoreData
import UIKit

class StorageDataServices {
    private let container: NSPersistentContainer
    private let containerName = "PokemonContainer"
    private let entityName = "PokemonEntity"
    
    static var shared = StorageDataServices()
    
    private init() {
        container = NSPersistentContainer(name: containerName)
        container.loadPersistentStores { (_, error) in
            if let error = error {
                print("Error leading Core Data\(error)")
            }
        }
    }
    
    func save(pokemones: [PokemonViewModel]) {
        for item in pokemones {
            save(pokemonViewModel: item)
        }
    }
    
    func save(pokemonViewModel: PokemonViewModel?) {
        guard let pokeViewModel = pokemonViewModel else { return }
        
        let pokemonEntity = PokemonEntity(context: container.viewContext)
        
        pokemonEntity.id = Int64(pokeViewModel.id)
        pokemonEntity.name = pokemonViewModel?.name
        pokemonEntity.mainUrlImage = pokeViewModel.mainUrlImage
        pokemonEntity.secondUrlImage = pokeViewModel.secondUrlImage
        pokemonEntity.url = pokeViewModel.url
        
        pokemonEntity.types = pokeViewModel.types.joined(separator: ",")
        pokemonEntity.moves = pokeViewModel.moves.joined(separator: ",")
        pokemonEntity.abilities = pokeViewModel.abilities.joined(separator: ",")
        
        commitTransaction()
    }
    
    func getPokemon(name: String) -> PokemonViewModel? {
        var pokemonVM: PokemonViewModel?
        if let pokemonEntity = searchPokemon(name: name) {
            pokemonVM = PokemonViewModel(pokemonEntity)
        }
        return pokemonVM
    }
    
    func getPokemones() -> [PokemonViewModel]? {
        var pokemones: [PokemonViewModel]?
        if let pokemonesEntity = getAllPokemones() {
            pokemones = pokemonesEntity.map({PokemonViewModel($0)})
        }
        return pokemones
    }
    
    func removePokemones() {
        self.clearStorage()
    }
}

extension StorageDataServices {
    private func commitTransaction() {
        save()
    }
    
    private func searchPokemon(name: String) -> PokemonEntity? {
        let request = NSFetchRequest<PokemonEntity>(entityName: entityName)
        var pokemonFound: PokemonEntity?
        
        do {
            try pokemonFound = container.viewContext.fetch(request)
                .first(where: {$0.name?.lowercased() == name.lowercased()})
        } catch {
            debugPrint("pokemon not found")
        }
        return pokemonFound
    }
    
    private func getAllPokemones() -> [PokemonEntity]? {
        let request = NSFetchRequest<PokemonEntity>(entityName: entityName)
        var pokemonFound: [PokemonEntity]?
        
        do {
            try pokemonFound = container.viewContext.fetch(request)
        } catch {
            debugPrint("pokemon not found")
        }
        return pokemonFound
    }
    
    private func clearStorage() {
        
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try container.viewContext.execute(deleteRequest)
        } catch let error as NSError {
            // TODO: handle the error
            debugPrint(error.localizedDescription)
        }
        save()
    }
    
    
    private func save() {
        do {
            try container.viewContext.save()
        } catch  {
            debugPrint(error.localizedDescription)
        }
    }
}

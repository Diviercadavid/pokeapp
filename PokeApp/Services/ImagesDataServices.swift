//
//  ImagesDataServices.swift
//  PokeApp
//
//  Created by Divier on 31/1/2024.
//

import Foundation
import UIKit

protocol ImagesDataServicesProtocol {
    func download(imageURL: String?, completion: @escaping(_ response: UIImage) -> Void)
}

class ImagesDataServices: ImagesDataServicesProtocol {
    
    func download(imageURL: String?, completion: @escaping(_ response: UIImage) -> Void) {
        NetworkingManager.download(imageURL: imageURL) { image, data, error, cacheType, isFinished, imageUrl in
            guard let image = image else { return }
            completion(image)
        }
    }
}

//
//  PokemonDataServices.swift
//  PokeApp
//
//  Created by Divier on 30/1/2024.
//

import Foundation
import Combine

protocol PokemonDataServicesProtocol {
    func getPokemonesByPage(offSet: Int?, completion: @escaping([PokemonViewModel]) -> Void)
    func getPokemonDetailBy(url: String, completion: @escaping(_ response: PokemonViewModel) -> Void)
}

class PokemonDataServices: PokemonDataServicesProtocol {
    
    @Published var error: Error? = nil
    
    private var baseURL: String = "https://pokeapi.co/api/v2/pokemon/"
    private var cancelableSubscription: AnyCancellable?
    
    deinit {
        cancelableSubscription = nil
    }
    
    func getPokemonesByPage(offSet: Int? = nil, completion: @escaping(_ response: [PokemonViewModel]) -> Void) {
        
        guard let url = URL(string: "\(baseURL)") else { return }
        
        let params = NetworkingManager.handlerPagination(offset: offSet)
        
        cancelableSubscription = NetworkingManager.download(url: url, params: params)?
            .decode(type: PokemonesListResponseObject.self, decoder: JSONDecoder())
            .map({$0.results.map({PokemonViewModel($0)})})
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: handlerError(completion:),
                  receiveValue: { response in
                completion(response)
            })
    }
    
    func getPokemonDetailBy(url: String, completion: @escaping(_ response: PokemonViewModel) -> Void) {
        
        guard let url = URL(string: url) else { return }
        
        cancelableSubscription = NetworkingManager.download(url: url)?
            .decode(type: PokemonDetailResponseObject.self, decoder: JSONDecoder())
            .map({PokemonViewModel($0)})
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: handlerError(completion:), receiveValue: { valueReturned in
                completion(valueReturned)
            })
    }
}

extension PokemonDataServices {
    private func handlerError(completion: Subscribers.Completion<Error>) {
        if case let .failure(error) = completion {
            NetworkingManager.handleCompletionError(error) { [weak self] handledError in
                guard let self = self else { return }
                self.error = handledError as Error
            }
        }
    }
}

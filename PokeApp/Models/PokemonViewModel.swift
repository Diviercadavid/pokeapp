//
//  PokemonViewModell.swift
//  PokeApp
//
//  Created by Divier on 1/2/2024.
//

import Foundation
import UIKit

struct PokemonViewModel: Hashable {
    
    var id: Int
    var name: String
    var url: String?
    var mainLiteralImage: UIImage?
    var secondLiteralImage: UIImage?
    var mainUrlImage: String
    var secondUrlImage: String
    var types: [String]
    var moves: [String]
    var abilities: [String]
    var typeStyle: ItemRowViewStyleEnum?
    
    init(id: Int,
         name: String,
         url: String,
         mainLiteralImage: UIImage?,
         secondLiteralImage: UIImage?,
         mainUrlImage: String,
         secondUrlImage: String,
         types: [String],
         moves: [String],
         abilities: [String],
         typeStyle: ItemRowViewStyleEnum) {
        
        self.id = id
        self.name = name
        self.url = url
        self.mainLiteralImage = mainLiteralImage
        self.secondLiteralImage = secondLiteralImage
        self.mainUrlImage = mainUrlImage
        self.secondUrlImage = secondUrlImage
        self.types = types
        self.moves = moves
        self.abilities = abilities
        self.typeStyle = typeStyle
        
    }
    
    init(_ object: PokemonDetailResponseObject) {
        self.id =  object.id ?? UUID().hashValue
        self.name = object.name.uppercased()
        self.types = object.types?.map({$0.type.name.capitalized}) ?? []
        self.moves = object.moves?.map({$0.move.name.capitalized}) ?? []
        self.abilities = object.abilities?.map({$0.ability.name.capitalized}) ?? []
        self.mainUrlImage = object.sprites?.other.officialArtwork.frontDefault ?? ""
        self.secondUrlImage = object.sprites?.other.dreamWorld.frontDefault ?? ""
        if self.url == nil {
            self.url = object.url ?? ""
        }
        self.setTypeStyle()
    }
    
    init(_ object: PokemonEntity) {
        self.id = Int(object.id)
        self.name = object.name ?? ""
        self.mainUrlImage = object.mainUrlImage ?? ""
        self.secondUrlImage = object.secondUrlImage ?? ""
        self.url = object.url
        self.types = object.types?.components(separatedBy: ",") ?? []
        self.moves = object.moves?.components(separatedBy: ",") ?? []
        self.abilities = object.abilities?.components(separatedBy: ",") ?? []
        self.setTypeStyle()
    }
    
    mutating private func setTypeStyle() {
        
        typeStyle = {
            switch types.first?.lowercased() {
            case "normal", "unknown":
                return .normal
            case "bug":
                return .bug
            case "fire":
                return .fire
            case "water":
                return .water
            case "grass":
                return .grass
            case "electric":
                return .electric
            case "psychic":
                return .psychic
            case "dragon":
                return .dragon
            case "flying":
                return .flying
            default:
                return .normal
            }
        }()
        
    }
    
    mutating func setImages(mainImage: UIImage? = nil, secondImage: UIImage? = nil) {
        self.mainLiteralImage = mainImage
        self.secondLiteralImage = secondImage
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        // You can include more properties here if needed
    }
    
    static func == (lhs: PokemonViewModel, rhs: PokemonViewModel) -> Bool {
        return lhs.id == rhs.id
        // You can include more properties here if needed
    }
}

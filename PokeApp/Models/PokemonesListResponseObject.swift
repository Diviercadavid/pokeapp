//
//  ResponseObject.swift
//  PokeApp
//
//  Created by Divier on 30/1/2024.
//

import Foundation

struct PokemonesListResponseObject: Codable {
    let count: Int
    let next: String
    let results: [PokemonDetailResponseObject]
}

// MARK: - Result
//struct PokemonNameResponseObject: Codable {
//    let name: String
//    let url: String
//}



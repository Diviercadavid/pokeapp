//
//  PokemonesDetail.swift
//  PokeApp
//
//  Created by Divier on 31/1/2024.
//

import Foundation

struct PokemonDetailResponseObject: Codable, Hashable {
    let abilities: [AbilityElement]?
    var id: Int? 
    let moves: [Move]?
    let name: String
    let sprites: Sprites?
    let types: [TypeElement]?
    let url: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.abilities = try container.decodeIfPresent([AbilityElement].self, forKey: .abilities)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.moves = try container.decodeIfPresent([Move].self, forKey: .moves)
        self.name = try container.decode(String.self, forKey: .name)
        self.sprites = try container.decodeIfPresent(Sprites.self, forKey: .sprites)
        self.types = try container.decodeIfPresent([TypeElement].self, forKey: .types)
        self.url = try container.decodeIfPresent(String.self, forKey: .url)
        
        if id == nil {
            id = UUID().hashValue
        }
    }
    
    init(abilities: [AbilityElement]?, id: Int? = nil, moves: [Move]?, name: String, sprites: Sprites?, types: [TypeElement]?, url: String?) {
        self.abilities = abilities
        self.id = id
        self.moves = moves
        self.name = name
        self.sprites = sprites
        self.types = types
        self.url = url
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        // You can include more properties here if needed
    }
    
    static func == (lhs: PokemonDetailResponseObject, rhs: PokemonDetailResponseObject) -> Bool {
        return lhs.id == rhs.id
        // You can include more properties here if needed
    }
}

// MARK: - AbilityElement
struct AbilityElement: Codable {
    let ability: MoveClass
    let isHidden: Bool
    let slot: Int
    
    enum CodingKeys: String, CodingKey {
        case ability
        case isHidden = "is_hidden"
        case slot
    }
}

// MARK: - MoveClass
struct MoveClass: Codable {
    let name: String
    let url: String
}

// MARK: - Move
struct Move: Codable {
    let move: MoveClass
    
    enum CodingKeys: String, CodingKey {
        case move
    }
}

// MARK: - Sprites
struct Sprites: Codable {
    let other: Other
    
    enum CodingKeys: String, CodingKey {
        case other
    }
}

// MARK: - Other
struct Other: Codable {
    let dreamWorld: DreamWorld
    let officialArtwork: OfficialArtwork
    
    enum CodingKeys: String, CodingKey {
        case dreamWorld = "dream_world"
        case officialArtwork = "official-artwork"
    }
}

// MARK: - DreamWorld
struct DreamWorld: Codable {
    let frontDefault: String
    
    enum CodingKeys: String, CodingKey {
        case frontDefault = "front_default"
    }
}

// MARK: - OfficialArtwork
struct OfficialArtwork: Codable {
    let frontDefault, frontShiny: String
    
    enum CodingKeys: String, CodingKey {
        case frontDefault = "front_default"
        case frontShiny = "front_shiny"
    }
}

// MARK: - TypeElement
struct TypeElement: Codable {
    let slot: Int
    let type: MoveClass
}

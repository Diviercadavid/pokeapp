//
//  ContentView.swift
//  PokeApp
//
//  Created by Divier on 30/1/2024.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundStyle(.tint)
            Text("Hello, world!")
        }
        .onTapGesture {
//            PokemonDataServices().getPokemonDetailBy(url: "https://pokeapi.co/api/v2/pokemon/21/")
        }
        .onAppear(perform: {
        })
        .padding()
    }
}

#Preview {
    ContentView()
}


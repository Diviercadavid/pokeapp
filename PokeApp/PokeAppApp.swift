//
//  PokeAppApp.swift
//  PokeApp
//
//  Created by Divier on 30/1/2024.
//

import SwiftUI
//✅ Create manager api
//✅ Connect to get Pokemones By Offset
//✅ Connect to get Pokemon Detail
//✅ Add cocoapods
//✅ Create listView to show many pokemones
//✅ Create viewModel to show many pokemones
//✅ Create download images
//✅ Create pagination
//✅ Create images handler to save
//✅ Create detailpokemonView
//✅ Create enum for Pokemones types
//✅ Create Controller to navigate between views
//✅ Create Manager to save data.
//✅ Handler offline state and show data saved
//✅ Create detailpokemonViewModel
//✅ Redesign in ItemRowView (like challenger)
// Implement search bar.
// Create UnitTest for Networking layer
// Create UITest (Optional)

@main
struct PokeAppApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView(homeViewModel: HomeViewModel(pokemonDataServices: PokemonDataServices()), navController: NavigationController())
        }
    }
}

//
//  UIScreen.swift
//  PokeApp
//
//  Created by Divier on 4/2/2024.
//

import UIKit
extension UIScreen{
   static let screenWidth = UIScreen.main.bounds.size.width
   static let screenHeight = UIScreen.main.bounds.size.height
   static let screenSize = UIScreen.main.bounds.size
}

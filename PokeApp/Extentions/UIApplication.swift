//
//  UIApplication.swift
//  PokeApp
//
//  Created by Divier on 4/2/2024.
//

import Foundation
import SwiftUI

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}

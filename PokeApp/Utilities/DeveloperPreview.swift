//
//  DeveloperPreview.swift
//  PokeApp
//
//  Created by Divier on 31/1/2024.
//

import Foundation
class DeveloperPreview {
    
    static let instance = DeveloperPreview()
    
    private init() { }
    
    let abilities: [AbilityElement] = [
        AbilityElement(ability: MoveClass(name: "overgrow", url: "https://pokeapi.co/api/v2/ability/65/"),
                       isHidden: false, slot: 1),
        AbilityElement(ability: MoveClass(name: "chlorophyll", url: "https://pokeapi.co/api/v2/ability/34/"),
                       isHidden: false, slot: 3)
    ]
    
    let moves: [Move] = [
        Move(move: MoveClass(name: "swords-dance", url: "https://pokeapi.co/api/v2/move/14/")),
        Move(move: MoveClass(name: "razor-wind", url: "https://pokeapi.co/api/v2/move/13/"))
    ]
    
    let sprites:Sprites = Sprites(other: Other(dreamWorld: DreamWorld(frontDefault:
                                                                        "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/1.svg"),
                                               officialArtwork: OfficialArtwork(frontDefault:
                                                                                    "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png",
                                                                                frontShiny: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/shiny/1.png")))
    
    let types: [TypeElement] = [TypeElement(slot: 1, type: MoveClass(name: "grass", url: "https://pokeapi.co/api/v2/type/12/")),
                                TypeElement(slot: 2, type: MoveClass(name: "poison", url: "https://pokeapi.co/api/v2/type/4/"))]
    
    let url: String = "https://pokeapi.co/api/v2/pokemon/1"
    
    var pokemon1: PokemonDetailResponseObject {
        get {
            return PokemonDetailResponseObject(abilities: abilities, id: 1, moves: moves, name: "bulbasaur", sprites: sprites, types: types, url: url)
        }
    }
}

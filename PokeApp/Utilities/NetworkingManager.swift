//
//  NetworkingManager.swift
//  PokeApp
//
//  Created by Divier on 30/1/2024.
//

import Foundation
import Combine
import SDWebImage

class NetworkingManager {
    
    static let itemsPerPage = 15
    
    static func download(url: URL, params: [URLQueryItem]? = nil) -> AnyPublisher<Data, any Error>? {
        
        guard var components = URLComponents(url: url, resolvingAgainstBaseURL: false) else { return nil }
        
        components.queryItems = params
        
        guard let componentsURL = components.url else { return nil}
        
        return URLSession.shared.dataTaskPublisher(for: componentsURL)
            .tryMap({try handlerURLResponse(output: $0, url: url)})
            .eraseToAnyPublisher()
    }
    
    static func download(imageURL: String?, completeBock: @escaping (Optional<UIImage>, Optional<Data>, Optional<any Error>, SDImageCacheType, Bool, Optional<URL>) -> ()) {
        guard let url = URL(string: imageURL ?? "") else { return }
        SDWebImageManager.shared.loadImage(
            with: url,
            options: .continueInBackground,
            progress: nil, completed: completeBock)
        
    }
    
    static func handlerURLResponse(output: URLSession.DataTaskPublisher.Output, url: URL) throws -> Data {
        guard let response = output.response as? HTTPURLResponse,
              response.statusCode >= 200 && response.statusCode < 300 else {
            throw CustomError.badURLResponse(url: url, data: output.data)
        }
        return output.data
    }
    
    static func handleCompletionError<T: Error>(_ error: T, onError: @escaping (Error) -> Void) {
        // Perform any logging or analytics here if needed.
        debugPrint("logging or analytics error \(error.localizedDescription)")
        onError(error)
    }
    
    static func handlerPagination(offset: Int? = 10) -> [URLQueryItem] {

        let parameters = [
            URLQueryItem(name: "offset", value: "\(offset ?? 0)"),
            URLQueryItem(name: "limit", value: "\(itemsPerPage)")
        ]
        
        return parameters
    }
}

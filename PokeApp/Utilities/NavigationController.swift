//
//  NavigationCotroller.swift
//  PokeApp
//
//  Created by Divier on 2/2/2024.
//

import SwiftUI

enum NavigationType: Hashable {
    case home
    case detail
    case about
}

// viewModel Manager of Navigation Path
class NavigationController: ObservableObject {
    @Published var path = NavigationPath()
    
    func navigateWithTag(tag: String) {
        path.append(tag)
    }
    
    func navigateBack() {
        path.removeLast()
    }
    
    func navigateToRoot() {
        path = NavigationPath()
    }
    
    func navigateTo(value: NavigationType){
        path.append(value)
    }
}

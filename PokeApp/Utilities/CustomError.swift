//
//  CustomError.swift
//  PokeApp
//
//  Created by Divier on 30/1/2024.
//

import Foundation

enum CustomError: LocalizedError {
    case badURLResponse(url: URL, data: Data)
    case coreData(description: String)
    case unknown
    
    var errorDescription: String? {
        switch self {
        case .badURLResponse(url: let url, data: let data):
            return "[🔥] Bad response from URL: \(url). \n \(String(data: data, encoding: String.Encoding.utf8) ?? "")"
        case .coreData(let description):
            return "\(description)"
        case .unknown:
            return "<⚠️> Unknown error occurred"
        }
    }
}
